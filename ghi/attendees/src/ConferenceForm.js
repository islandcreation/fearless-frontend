import React, { useEffect, useState } from 'react';

function ConferenceForm () {
  const [locations, setLocations] = useState([])

  const [formData, setFormData] = useState({
    name: '',
    starts: '',
    ends: '',
    description: '',
    max_presentations: '',
    max_attendees: '',
    location: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect (() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    // console.log(formData)
    event.preventDefault();

    const url = 'http://localhost:8000/api/conferences/';

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setFormData({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

// console.log(JSON.stringify(formData))
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.starts} placeholder="Starts" required type="date" id="starts" name="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.ends} placeholder="Ends" required type="date" id="ends" name="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>

              <div className="form-floating mb-3">
                <textarea onChange={handleFormChange} value={formData.description} placeholder="Description" required type="text" id="description" name="description" className="form-control" rows="3"></textarea>
                <label htmlFor="description">Description</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.max_presentations} placeholder="Max Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.max_attendees} placeholder="Max Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>

              <div className="mb-3">
                <select onChange={handleFormChange} value={formData.location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>{location.name}</option>
                    )
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>

            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ConferenceForm;
